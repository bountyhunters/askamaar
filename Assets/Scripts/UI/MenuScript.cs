﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// Take care of menu navigation and window control
/// </summary>
/// 
/// <author>Frederik Mortensen</author>
namespace Askamaar
{

    public class MenuScript : AbstractNavigableGui
    {

        public Button inventoryButton;
        private Button characterButton;
        private Button optionsButton;
        private Button gameButton;

        /// <summary>
        /// get menu controls and set focus
        /// </summary>
        public void Start()
        {
            Button[] buttons = gameObject.GetComponentsInChildren<Button>();

            foreach (Button button in buttons)
            {
                if (button.name.Equals("InventoryButton"))
                {
                    inventoryButton = button;
                }
                else if (button.name.Equals("CharacterButton"))
                {
                    characterButton = button;
                }
                else if (button.name.Equals("OptionsButton"))
                {
                    optionsButton = button;
                }
                else if (button.name.Equals("GameButton"))
                {
                    gameButton = button;
                }
                else
                {
                    Debug.LogError("Don´t know this button! New?");
                }
            }

            //Setting focus to first button
            inventoryButton.Select();
        }

        /// <summary>
        /// react to input
        /// </summary>
        public override void Update()
        {
            base.Update();
            //pressed playstation x button
            if (Input.GetButtonUp("PS4_CROSS") || Input.GetButtonUp("Submit"))
            {
                OpenSelectedWindow();
            }
        }

        /// <summary>
        /// opens the chosen gui window based on the button pressed
        /// </summary>
        private void OpenSelectedWindow()
        {
            //open the chosen window 
            GameObject selectedGameObject = EventSystem.current.currentSelectedGameObject;
            Button selected = selectedGameObject.GetComponent<Button>();
            if (selected == null)
            {
                Debug.LogError("No button found!");
            }

            if (selected.Equals(inventoryButton))
            {
                UIControllerScript.Instance.OpenInventoryWindow();
            }
            else if (selected.Equals(characterButton))
            {
                Debug.Log("Opening " + selected.name);
                //UIControllerScript.Instance.OpenCharacterWindow();
            }
            else if (selected.Equals(optionsButton))
            {
                Debug.Log("Opening " + selected.name);
                UIControllerScript.Instance.OpenSaveGameWindow(); //TODO: misused for testing reasons.... should open options, not save!
            }
            else if (selected.Equals(gameButton))
            {
                UIControllerScript.Instance.OpenConfirmWindow("Zurück ins Hauptmenü?\nNicht gespeicherter Fortschritt geht verloren.");
            }
            else
            {
                Debug.LogError("Button unknown!  " + selected.name);
            }
        }

    }
}