﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

/// <summary>
/// Main menu navigation, game loading...
/// </summary>
/// 
/// <author>Frederik Mortensen</author>
namespace Askamaar
{

    public class MainMenuScript : AbstractNavigableGui
    {

        private Text mainTitle;
        private Image backgroundPanel;
        private GameObject navigationPanel;
        private Text mainTitleOnLeft;
        public Button continueButton;
        private Button newButton;
        private Button loadButton;
        private Button optionsButton;
        private Button creditsButton;
        private Button exitButton;
        private Text xKeyLabel;
        private GameObject continueButtonObject;
        private SaveGameEntity[] savegames;
        private int latestSavegame = -1;

        private MainMenuController mainMenuController = MainMenuController.Instance;

        /// <summary>
        /// initially find buttons and other objects and assign to local variables
        /// </summary>
        public void Awake()
        {
            navigationPanel = GameObject.Find("NavigationPanel");

            Button[] buttons = gameObject.GetComponentsInChildren<Button>();
            foreach (Button button in buttons)
            {
                switch (button.name)
                {
                    case "ContinueButton":
                        continueButton = button;
                        break;
                    case "NewButton":
                        newButton = button;
                        break;
                    case "LoadButton":
                        loadButton = button;
                        break;
                    case "OptionsButton":
                        optionsButton = button;
                        break;
                    case "CreditsButton":
                        creditsButton = button;
                        break;
                    case "ExitButton":
                        exitButton = button;
                        break;
                    default:
                        throw new System.Exception("Unknown button found!" + button.name);
                }
            }

            Text[] texts = gameObject.GetComponentsInChildren<Text>();
            foreach (Text text in texts)
            {
                switch (text.name)
                {
                    case "XKeyLabel":
                        xKeyLabel = text;
                        break;
                    case "MainTitle":
                        mainTitle = text;
                        break;
                    case "MainTitleOnLeft":
                        mainTitleOnLeft = text;
                        break;
                    case "Text":
                        //button text labels are ignored
                        break;
                    default:
                        throw new System.Exception("Unknown text label found!" + text.name);
                }
            }

            Image[] images = gameObject.GetComponentsInChildren<Image>();
            foreach (Image image in images)
            {
                switch (image.name)
                {
                    case "BackgroundPanel":
                        backgroundPanel = image;
                        break;

                    //these are ignored
                    case "ContinueButton":
                    case "NewButton":
                    case "LoadButton":
                    case "OptionsButton":
                    case "CreditsButton":
                    case "ExitButton":
                        break;
                    default:
                        throw new System.Exception("Unknown image label found! " + image.name);
                }
            }

            continueButtonObject = navigationPanel.transform.Find("ContinueButton").gameObject;

        }

        /// <summary>
        /// initializing
        /// </summary>
        public void Start()
        {
            CloseNavigationPanel();
            savegames = SaveGameController.Instance.LoadAllSaveGames();
            bool savegameFound = false;
            for (int i = 0; i < savegames.Length; i++)
            {
                if (savegames[i] != null)
                {
                    savegameFound = true;
                    if (latestSavegame == -1)
                        latestSavegame = i;
                    else
                    {
                        if (savegames[i].date > savegames[latestSavegame].date)
                            latestSavegame = i;
                    }
                }
            }
            if (!savegameFound)
            {
                continueButtonObject.SetActive(false);
            }
        }

        /// <summary>
        /// check user input and handle it
        /// </summary>
        public override void Update()
        {
            base.Update();
            HandleUserInput();
        }

        private void SelectFirstNavigationButton()
        {
            if (continueButtonObject.activeSelf)
            {
                continueButton.Select();
            }
            else
            {
                newButton.Select();
            }
        }

        /// <summary>
        /// called in update method to handle the buttons pressed via user controller
        /// </summary>
        private void HandleUserInput()
        {
            //pressed playstation x button
            if (Input.GetButtonUp("PS4_CROSS") || Input.GetButtonUp("Submit"))
            {
                OpenSelectedWindow();
            }
            else if (Input.GetButtonUp("PS4_CIRCLE"))
            {
                CloseNavigationPanel();
            }
        }

        /// <summary>
        /// opens the chosen gui window based on the button pressed
        /// </summary>
        private void OpenSelectedWindow()
        {
            //if not even navigation is open, then... open it
            if (navigationPanel.activeSelf == false)
            {
                OpenNavigationPanel();
                return;
            }

            //else open the chosen window 
            GameObject selectedGameObject = EventSystem.current.currentSelectedGameObject;

            if (selectedGameObject == null)
                return;

            Button selected = selectedGameObject.GetComponent<Button>();
            if (selected == null)
            {
                Debug.LogError("No button found!");
            }

            if (selected.name.Equals("ContinueButton"))
            {
                SaveGameController.Instance.LoadGame(latestSavegame);
            }
            else if (selected.name.Equals("NewButton"))
            {
                SceneManager.LoadScene("FirstVillage");
            }
            else if (selected.name.Equals("LoadButton"))
            {
                mainMenuController.OpenSaveGamesWindow();
            }
            else if (selected.name.Equals("OptionsButton"))
            {
                //TODO
            }
            else if (selected.name.Equals("CreditsButton"))
            {
                mainMenuController.OpenCreditsWindow();
            }
            else if (selected.name.Equals("ExitButton"))
            {
                //only works with actual build, not inside unity
                Application.Quit();
            }
            else
            {
                Debug.LogError("Button unknown!  " + selected.name);
            }
        }

        /// <summary>
        /// shows the navigation buttons new, load...
        /// </summary>
        public void OpenNavigationPanel()
        {
            mainTitle.enabled = false;
            xKeyLabel.enabled = false;
            backgroundPanel.enabled = true;
            mainTitleOnLeft.enabled = true;
            navigationPanel.SetActive(true);
            //Bugfix: First deselect, then reselect needed.
            //Else it would only get selected the first time
            EventSystem.current.SetSelectedGameObject(null);
            SelectFirstNavigationButton();
        }

        /// <summary>
        /// hides the navigation buttons load, options...
        /// </summary>
        private void CloseNavigationPanel()
        {
            mainTitle.enabled = true;
            mainTitleOnLeft.enabled = false;
            navigationPanel.SetActive(false);
            backgroundPanel.enabled = false;
            xKeyLabel.enabled = true;
        }

    }
}