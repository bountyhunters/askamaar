﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Takes care of showing the gui and opening and closing windows
/// </summary>
/// 
/// <author>Frederik Mortensen</author>
namespace Askamaar
{
    public class UIControllerScript : MonoBehaviour
    {

        private static UIControllerScript instance;
        public static UIControllerScript Instance
        {
            get
            {
                return instance;
            }
        }

        //list of possible windows
        public GameObject guiWindow;
        public GameObject menuWindow;
        public GameObject inventoryWindow;
        public GameObject confirmDialogueWindow;
        public GameObject saveGameDialogWindow;
        public GameObject backgroundWindow;

        private Transform actionsPanel;
        private SaveGamesGuiScript saveGamesGuiScript;
        public InventoryScript inventory;
        private Transform topPanel;
        private ScreenTextScript screenText;


        /// <summary>
        /// initializing singleton, gui window and stuff
        /// </summary>
        void Awake()
        {

            //ensure that only one instance of this script is running
            if (instance != null)
            {
                Debug.LogWarning("This singleton already exists!");
                Destroy(this);
            }
            else
            {
                instance = this;
            }

            //instantiate all gui windows that exist
            guiWindow = (GameObject)Instantiate(guiWindow);
            if (guiWindow == null)
            {
                Debug.LogError("Prefab UICanvas is not attached to UIController prefab. Did you change the prefab?");
            }
            topPanel = guiWindow.transform.Find("TopPanel");
            screenText = topPanel.GetComponent<ScreenTextScript>();
            menuWindow = (GameObject)Instantiate(menuWindow);
            if (menuWindow == null)
            {
                Debug.LogError("Prefab MenuCanvas is not attached to UIController prefab. Did you change the prefab?");
            }
            backgroundWindow = (GameObject)Instantiate(backgroundWindow);
            if (backgroundWindow == null)
            {
                Debug.LogError("Prefab BackgroundCanvas is not attached to UIController prefab. Did you change the prefab?");
            }
            inventoryWindow = (GameObject)Instantiate(inventoryWindow);
            inventory = inventoryWindow.GetComponentInChildren<InventoryScript>();
            if (inventoryWindow == null)
            {
                Debug.LogError("Prefab InventoryCanvas is not attached to UIController prefab. Did you change the prefab?");
            }
            confirmDialogueWindow = (GameObject)Instantiate(confirmDialogueWindow);
            if (confirmDialogueWindow == null)
            {
                Debug.LogError("Prefab ConfirmDialogueCanvas is not attached to UIController prefab. Did you change the prefab?");
            }

            saveGameDialogWindow = Instantiate(saveGameDialogWindow);
            if (saveGameDialogWindow == null)
            {
                Debug.LogError("Error while loading savegame dialog window. Did the prefab path change?");
            }
            saveGamesGuiScript = saveGameDialogWindow.GetComponentInChildren<SaveGamesGuiScript>();

            actionsPanel = guiWindow.transform.Find("ActionsPanel");
        }

        /// <summary>
        /// handle the windows to be opened at first
        /// </summary>
        void Start()
        {
            //initally only gui window is open
            CloseAllWindows();
            OpenGuiWindow();
        }

        /// <summary>
        /// handling user input
        /// </summary>
        void Update()
        {
            //if key pressed, open menu panel
            if (Input.GetButtonUp("PS4_TOUCHPAD") || Input.GetKeyUp(KeyCode.M) || Input.GetKeyUp("escape"))
            {
                ToggleMenuWindow();
            }
            else if (Input.GetButtonUp("PS4_CIRCLE") || Input.GetKeyUp("backspace"))
            {
                GoBack();
            }
        }

        /// <summary>
        /// closes all and only shows the gui window
        /// </summary>
        public void OpenGuiWindow()
        {
            CloseAllWindows();
            backgroundWindow.SetActive(false);
            guiWindow.SetActive(true);
            if (screenText.topText != null)
            {
                screenText.fadeTopText(screenText.topText.text, 0f);
            }

        }

        /// <summary>
        /// open the inventory window, you know.
        /// </summary>
        public void OpenInventoryWindow()
        {
            CloseAllWindows();
            inventoryWindow.SetActive(true);
        }

        /// <summary>
        /// open the menu window
        /// </summary>
        private void OpenMenuWindow()
        {
            CloseAllWindows();
            backgroundWindow.SetActive(true);
            menuWindow.SetActive(true);
            MenuScript script = menuWindow.GetComponent<MenuScript>();
            if (script != null && script.inventoryButton != null)
            {
                script.inventoryButton.Select();
            }
        }

        /// <summary>
        /// open a confirmation dialogue with the given text as question
        /// </summary>
        /// <param name="questionText"></param>
        public void OpenConfirmWindow(string questionText)
        {
            CloseAllWindows();
            confirmDialogueWindow.SetActive(true);
            Text question = confirmDialogueWindow.GetComponentInChildren<Text>();
            question.text = questionText;
        }

        /// <summary>
        /// open the dialog to save the current game
        /// </summary>
        public void OpenSaveGameWindow()
        {
            CloseAllWindows();
            saveGameDialogWindow.SetActive(true);
            saveGamesGuiScript.saveLoadMode = true; // open save mode
        }

        /// <summary>
        /// open the dialog to load the current game
        /// </summary>
        public void OpenLoadGameWindow()
        {
            CloseAllWindows();
            saveGameDialogWindow.SetActive(true);
            saveGamesGuiScript.saveLoadMode = false; // open load mode
        }

        /// <summary>
        /// opens and closes menu window
        /// </summary>
        public void ToggleMenuWindow()
        {
            if (menuWindow.activeSelf || inventoryWindow.activeSelf)
            {
                OpenGuiWindow();
            }
            else
            {
                CloseAllWindows();
                backgroundWindow.SetActive(true);
                menuWindow.SetActive(true);
                MenuScript script = menuWindow.GetComponent<MenuScript>();
                if (script != null && script.inventoryButton != null)
                {
                    script.inventoryButton.Select();
                }
            }
        }

        /// <summary>
        /// Go back one step in menu. If submenu is opened, go to menu, else close menu.
        /// </summary>
        public void GoBack()
        {
            if (inventoryWindow.activeSelf)
            {
                OpenMenuWindow();
            }
            else if (menuWindow.activeSelf)
            {
                OpenGuiWindow();
            }
            else if (confirmDialogueWindow.activeSelf)
            {
                OpenMenuWindow();
            }
            else if (saveGameDialogWindow.activeSelf)
            {
                OpenMenuWindow();
            }
        }

        /// <summary>
        /// closes all gui windows
        /// </summary>
        public void CloseAllWindows()
        {
            menuWindow.SetActive(false);
            guiWindow.SetActive(false);
            inventoryWindow.SetActive(false);
            confirmDialogueWindow.SetActive(false);
            saveGameDialogWindow.SetActive(false);
        }

        /// <summary>
        /// fills the inventory from savegame
        /// </summary>
        /// <param name="slots">serialized inventory item data</param>
        public void fillInventory(string[] slots)
        {
            OpenInventoryWindow();
            InventoryScript inventory = inventoryWindow.GetComponentInChildren<InventoryScript>();
            if (slots != null)
                inventory.fillInventory(slots);
            OpenGuiWindow();
        }

    }
}