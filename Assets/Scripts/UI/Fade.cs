﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 
/// </summary>
/// 
/// <author>Frederik Mortensen</author>
namespace Asamaar
{
    class Fade
    {

        /// <summary>
        /// fades from the fromColor to the toColor.
        /// Can be fading from transparent to opaque or vice versa in example.
        /// </summary>
        /// <param name="target">The image or text to fade</param>
        /// <param name="initialWait">seconds to wait before fading</param>
        /// <param name="duration">defines the end of the fade time</param>
        /// <param name="fromColor">start point</param>
        /// <param name="toColor">end point</param>
        /// <returns></returns>
        public IEnumerator ExecuteFade(MaskableGraphic target, float initialWait, float duration, Color fromColor, Color toColor)
        {
            while ((initialWait = initialWait - Time.deltaTime) > 0.0f)
            {
                yield return (0);
            }
            Debug.Log("Starting");
            float elapsedTime = 0;
            while (elapsedTime < 1)
            {
                target.color = Color.Lerp(fromColor, toColor, elapsedTime);
                elapsedTime += Time.deltaTime / duration;
                yield return null;
            }
        }
    }
}
