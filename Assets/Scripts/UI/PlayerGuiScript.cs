﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

/// <summary>
/// Script that marks the current player in gui, refreshes health bar and stuff like that.
/// </summary>
/// 
/// <author>Frederik Mortensen</author>
namespace Askamaar
{

    public class PlayerGuiScript : MonoBehaviour
    {

    //    public Slider healthBar;
    //    public Text healthBarText;
    //    public Image statusImage;
    //    public RectTransform fillArea;
    //    public RectTransform handleSlideArea;

    //    private Player player;
    //    private Creature currentMasterPlayer;
    //    private VerticalLayoutGroup verticalLayoutGroup;

    //    private Sprite statusFreeSprite;
    //    private Sprite statusMoveSprite;
    //    private Sprite statusAttackSprite;
    //    private Sprite statusSelectSprite;

    //    // Use this for initialization, loading levelmaster and player entities
    //    void Start()
    //    {
    //        statusFreeSprite = Resources.Load<Sprite>("Sprites/IngameGui/status_free");
    //        statusMoveSprite = Resources.Load<Sprite>("Sprites/IngameGui/status_move");
    //        statusAttackSprite = Resources.Load<Sprite>("Sprites/IngameGui/status_attack");
    //        statusSelectSprite = Resources.Load<Sprite>("Sprites/IngameGui/status_select");
    //    }

    //    /// <summary>
    //    /// Actualizing the player health bars, action points and stuff.
    //    /// </summary>
    //    void OnGUI()
    //    {

    //                    healthBar.value = thief.GetHealth();
    //                    healthBarText.text = thief.GetHealth() + "/" + thief.GetMaxHealth();
    //                    if (currentMasterPlayer.gameObject.name.Contains("Thief"))
    //                    {
    //                        verticalLayoutGroup.childForceExpandWidth = true;
    //                        statusImage.enabled = true;
    //                        fillArea.sizeDelta = new Vector2(141, 20);
    //                        fillArea.localPosition = new Vector3(74, 0, 0);
    //                        handleSlideArea.sizeDelta = new Vector2(141, 20);
    //                        handleSlideArea.localPosition = new Vector3(74, 0, 0);
    //                    }
    //                    else
    //                    {
    //                        verticalLayoutGroup.childForceExpandWidth = false;
    //                        statusImage.enabled = false;
    //                        fillArea.sizeDelta = new Vector2(100, 20);
    //                        fillArea.localPosition = new Vector3(50, 0, 0);
    //                        handleSlideArea.sizeDelta = new Vector2(100, 20);
    //                        handleSlideArea.localPosition = new Vector3(50, 0, 0);
    //                    }

    //                    //actualize status image
    //                    if (!levelMaster.GetComponent<LevelMaster>().IsEncounterActive())
    //                    {
    //                        statusImage.sprite = statusFreeSprite;
    //                    }
    //                    else
    //                    {
    //                        if (thief.isEncounterStatusMoving())
    //                        {
    //                            statusImage.sprite = statusMoveSprite;
    //                        }
    //                        else if (thief.isEncounterStatusAttacking() || thief.isEncounterStatusAttackPending())
    //                        {
    //                            statusImage.sprite = statusAttackSprite;
    //                        }
    //                        else if (thief.isEncounterStatusAttackSelection())
    //                        {
    //                            statusImage.sprite = statusSelectSprite;
    //                        }
    //                    }
    //                    break;
    //    }
    }
}