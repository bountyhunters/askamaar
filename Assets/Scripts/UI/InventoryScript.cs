﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// Handles the items the players have in their bag
/// </summary>
/// 
/// <author>Frederik Mortensen</author>
namespace Askamaar
{

    public class InventoryScript : AbstractGridWindowScript
    {

        //used when player sorts his inventory and moves items around
        private Slot sourceSlot, targetSlot;
        private ItemDatabase itemDB;

        /// <summary>
        /// initializing all slots and rows and stuff
        /// </summary>
        public void Awake()
        {
            if (initialized)
                return;

            //initially create slots and hold them as variable
            slots = 48;
            rows = 6;
            slotPaddingLeft = 0;
            slotPaddingTop = 0;
            slotWidth = 144;
            slotHeight = 144;
            textAlignForSlot = TextAnchor.LowerRight;
            CreateLayout();
            firstSlot.GetComponent<Button>().Select();
            itemDB = new ItemDatabase();
            initialized = true;
        }

        /// <summary>
        /// react to input and handle navigation in example
        /// </summary>
        public override void Update()
        {
            base.Update();
            selected = EventSystem.current.currentSelectedGameObject;
            if (selected == null)
            {
                //mouse was clicked somewhere. Should be removed when all mouse occurences are removed from game. Because
                //then this must not happen anymore
                return;
            }

            if (Input.GetButtonUp("PS4_CROSS") || Input.GetButtonUp("Submit"))
            {
                MoveItem(selected);
            }

            if (Input.GetButtonUp("PS4_SQUARE") || Input.GetKeyUp(KeyCode.U))
            {
                selected.GetComponent<Slot>().UseItem();
            }

        }

        /// <summary>
        /// Adds the item the player was given or collided to the player bag. Also checks if a new slot is needed or item is stackable.
        /// </summary>
        /// <param name="item">the new item</param>
        /// <returns>wether the item could be added or not</returns>
        public bool AddItem(Item item)
        {

            if (item.stackableSize == 1)
                return PlaceEmpty(item);
            else
            {
                //try to find slots that could take this stackable item
                foreach (GameObject slot in allSlots)
                {
                    Slot slotRef = slot.GetComponent<Slot>();
                    if (!slotRef.IsEmpty())
                    {
                        if (slotRef.CurrentItem.type == item.type && slotRef.IsAvailable)
                        {
                            slotRef.AddItem(item);
                            return true;
                        }
                    }
                }
                //no slot found, place into new slot
                if (EmptySlots > 0)
                {
                    return PlaceEmpty(item);
                }
            }
            Debug.LogError("Inventory full");
            return false;
        }

        /// <summary>
        /// Add item to next free slot, no stack possible
        /// </summary>
        /// <param name="item">the item that was picked up and needs to be placed into a slot</param>
        /// <returns>true if item was placed</returns>
        private bool PlaceEmpty(Item item)
        {
            if (EmptySlots > 0)
            {
                foreach (GameObject slot in allSlots)
                {
                    //getting slot script component
                    Slot slotScript = slot.GetComponent<Slot>();
                    if (slotScript.IsEmpty())
                    {
                        slotScript.AddItem(item);
                        EmptySlots--;
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// items the player wants to move around to sort his repository
        /// </summary>
        /// <param name="selected">the selected item in the inventory</param>
        public void MoveItem(GameObject selected)
        {
            if (sourceSlot == null)
            {
                if (!selected.GetComponent<Slot>().IsEmpty())
                {
                    sourceSlot = selected.GetComponent<Slot>();
                    sourceSlot.GetComponent<Image>().color = Color.gray;
                }
            }
            else if (targetSlot == null)
            {
                targetSlot = selected.GetComponent<Slot>();
            }
            if (sourceSlot != null && targetSlot != null)
            {
                Stack<Item> actualTargetItems = new Stack<Item>(targetSlot.StackItems);
                targetSlot.AddItems(sourceSlot.StackItems);

                if (actualTargetItems.Count == 0)
                {
                    sourceSlot.ClearSlot();
                }
                else
                {
                    sourceSlot.AddItems(actualTargetItems);
                }

                sourceSlot.GetComponent<Image>().color = Color.white;

                sourceSlot = null;
                targetSlot = null;
            }
        }

        /// <summary>
        /// always set focus to first slot when coming from menu to inventory
        /// </summary>
        void OnGUI()
        {
            if (EventSystem.current.currentSelectedGameObject != null
                && EventSystem.current.currentSelectedGameObject.name.Equals("InventoryButton"))
            {
                EventSystem.current.SetSelectedGameObject(firstSlot);
            }
        }

        /// <summary>
        /// serialization helper
        /// </summary>
        /// <returns>returns all items as string representation holding the most important values in a fixed order</returns>
        public string[] GetAllItems()
        {
            if (allSlots == null || allSlots.Count <= 0)
                return null;

            string[] slots = new string[allSlots.Count];
            for (int i = 0; i < allSlots.Count; i++)
            {
                Slot slot = allSlots[i].GetComponent<Slot>();

                if (slot.StackItems == null)
                {
                    continue;
                }

                int index = slot.Index;
                int count = slot.StackItems.Count;
                if (count == 0)
                    continue;
                Item item = slot.StackItems.Peek();
                string itemString = index + ";" + count + ";" + item.id;
                slots[i] = itemString;
            }
            return slots.Length > 0 ? slots : null;
        }

        /// <summary>
        /// deserializing savegame content
        /// </summary>
        /// <param name="slots">the data to generate the items and fill the inventory from</param>
        public void fillInventory(string[] slots)
        {
            for (int i = 0; i < slots.Length; i++)
            {
                string slot = slots[i];
                if (string.IsNullOrEmpty(slot))
                {
                    continue;
                }
                string[] entries = slot.Split(';');
                if (entries.Length != 3)
                {
                    Debug.LogError("Wrong savegame format!");
                    return;
                }

                string slotIndex = entries[0];
                int count = int.Parse(entries[1]);
                int itemId = int.Parse(entries[2]);

                bool found = false;

                foreach (GameObject slotObject in allSlots)
                {
                    Slot currentSlot = slotObject.GetComponent<Slot>();
                    if (currentSlot.Index.Equals(int.Parse(slotIndex)))
                    {
                        Item item = itemDB.GetItemById(itemId);
                        item.id = itemId;
                        for (int j = 0; j < count; j++)
                        {
                            currentSlot.AddItem(item);
                        }

                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    Debug.LogError("Wrong savegame format!");
                }
            }
        }

    }
}