﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// 
/// </summary>
/// 
///<author>Frederik Mortensen</author>
namespace Askamaar
{
    public class AbstractNavigableGui : MonoBehaviour
    {

        private float joystickSensitivity = 0.8f;
        private float targetDelay = 0.1f;
        private float delay;

        private float horizontalJoystickInput;
        private float verticalJoystickInput;

        protected GameObject selected;

        /// <summary>
        /// default constructor initializing values
        /// </summary>
        public AbstractNavigableGui()
        {
            delay = targetDelay;
        }

        /// <summary>
        /// update user input
        /// </summary>
        public virtual void Update()
        {
            horizontalJoystickInput = Input.GetAxis("LeftH");
            verticalJoystickInput = Input.GetAxis("LeftV");
            selected = EventSystem.current.currentSelectedGameObject;

            if (selected == null)
                return; //mouse clicked somewhere

            HandleArrowNavigation();
            HandleJoystickNavigation(horizontalJoystickInput, verticalJoystickInput);
        }

        /// <summary>
        /// handle controller arrow keys
        /// </summary>
        public void HandleArrowNavigation()
        {

            Selectable currentSelectable = selected.GetComponent<Button>();
            if (currentSelectable == null)
                return;

            Selectable target = null;

            delay -= Time.deltaTime;
            if (delay <= 0)
            {
                if (Input.GetAxis("PS4_LEFT") == -1)
                {
                    target = currentSelectable.FindSelectableOnLeft();
                }

                if (Input.GetAxis("PS4_RIGHT") == 1)
                {
                    target = currentSelectable.FindSelectableOnRight();
                }

                if (Input.GetAxis("PS4_UP") == 1)
                {
                    target = currentSelectable.FindSelectableOnUp();
                }

                if (Input.GetAxis("PS4_DOWN") == -1)
                {
                    target = currentSelectable.FindSelectableOnDown();
                }

                if (target != null)
                {
                    target.Select();
                    HandleSelection();
                }

                delay = targetDelay;
            }
        }

        /// <summary>
        /// select the slot in the given direction
        /// </summary>
        public void HandleJoystickNavigation(float horizontalJoystickInput, float verticalJoystickInput)
        {

            Selectable currentSelectable = selected.GetComponent<Button>();
            if (currentSelectable == null)
                return;

            Selectable target = null;

            delay -= Time.deltaTime;
            if (delay <= 0)
            {

                if (horizontalJoystickInput < -joystickSensitivity)
                {
                    target = currentSelectable.FindSelectableOnLeft();
                }
                else if (horizontalJoystickInput > joystickSensitivity)
                {
                    target = currentSelectable.FindSelectableOnRight();
                }
                if (verticalJoystickInput < -joystickSensitivity)
                {
                    target = currentSelectable.FindSelectableOnUp();
                }
                else if (verticalJoystickInput > joystickSensitivity)
                {
                    target = currentSelectable.FindSelectableOnDown();
                }
                if (target != null)
                {
                    target.Select();
                    HandleSelection();
                }
                delay = targetDelay;
            }
        }

        /// <summary>
        /// inherited scripts can use this hook to react on selection changing
        /// </summary>
        public virtual void HandleSelection()
        {
            // can be overridden
        }
    }
}