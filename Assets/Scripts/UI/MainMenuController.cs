﻿using UnityEngine;

/// <summary>
/// Takes care of showing the windows inside the main menu
/// </summary>
/// 
/// <author>Frederik Mortensen</author>
namespace Askamaar
{

    public class MainMenuController : MonoBehaviour
    {
        
        private static MainMenuController instance;
        public static MainMenuController Instance
        {
            get
            {
                return instance;
            }
        }

        //list of possible windows
        public GameObject mainMenuWindow;
        public GameObject saveGameWindow;
        public GameObject creditsWindow;

        /// <summary>
        /// initializing singleton and window instances
        /// </summary>
        void Awake()
        {
            //ensure that only one instance of this script is running
            if (instance != null)
            {
                Debug.LogWarning("This singleton already exists!");
                Destroy(this);
            }
            else
            {
                instance = this;
            }

            //instantiate all windows that exist
            mainMenuWindow = Instantiate(mainMenuWindow);
            if (mainMenuWindow == null)
            {
                Debug.LogError("Prefab MainMenuWindow is not attached to MainMainuController prefab. Did you change the prefab?");
            }
            creditsWindow = (GameObject)Instantiate(creditsWindow);
            if (creditsWindow == null)
            {
                Debug.LogError("Prefab CreditsWindow is not attached to MainMainuController prefab. Did you change the prefab?");
            }
            saveGameWindow = (GameObject)Instantiate(saveGameWindow);
            if (saveGameWindow == null)
            {
                Debug.LogError("Prefab SaveGameWindow is not attached to MainMainuController prefab. Did you change the prefab?");
            }
        }

        /// <summary>
        /// reseting ui view and open the starting window
        /// </summary>
        void Start()
        { 
            //initally only main menu window is open
            CloseAllWindows();
            OpenMainMenuWindow();
        }

        /// <summary>
        /// handling user input
        /// </summary>
        void Update()
        {
            if (Input.GetButtonUp("PS4_CIRCLE") || Input.GetKeyUp("backspace"))
            {
                GoBack();
            }
        }

        /// <summary>
        /// open the main menu window
        /// </summary>
        public void OpenMainMenuWindow()
        {
            CloseAllWindows();
            mainMenuWindow.SetActive(true);
            MainMenuScript script = mainMenuWindow.GetComponent<MainMenuScript>();
            script.OpenNavigationPanel();
        }

        /// <summary>
        /// open the window to load or save savegames
        /// </summary>
        public void OpenSaveGamesWindow()
        {
            CloseAllWindows();
            saveGameWindow.SetActive(true);
        }

        /// <summary>
        /// open the credits page
        /// </summary>
        public void OpenCreditsWindow()
        {
            CloseAllWindows();
            creditsWindow.SetActive(true);
        }

        /// <summary>
        /// Go back one step in menu. If submenu is opened, go to menu, else close menu.
        /// </summary>
        public void GoBack()
        {
            if (mainMenuWindow.activeSelf)
            {
                //nothing to do
            } else
            {
                OpenMainMenuWindow();
            }
        }

        /// <summary>
        /// closes all opened canvas objects
        /// </summary>
        public void CloseAllWindows()
        {
            mainMenuWindow.SetActive(false);
            saveGameWindow.SetActive(false);
            creditsWindow.SetActive(false);
        }

    }
}