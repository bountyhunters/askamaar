﻿using UnityEngine;

/// <summary>
/// Show area name as gui text fade
/// </summary>
/// 
/// <author>Frederik Mortensen</author>
namespace Askamaar
{

    public class GuiTextFadeTriggerScript : MonoBehaviour
    {

        public string text;
        private ScreenTextScript script;

        /// <summary>
        /// initializing
        /// </summary>
        void Start()
        {
            GameObject guiWindow = UIControllerScript.Instance.guiWindow;
            ScreenTextScript[] scripts = guiWindow.GetComponentsInChildren<ScreenTextScript>();
            if (scripts.Length != 2)
                throw new System.ArgumentException("Can´t handle this. Was there a new script added?");
            script = scripts[0].name == "TopPanel" ? scripts[0] : scripts[1];
        }

        /// <summary>
        /// Show area name as gui text fade
        /// </summary>
        /// <param name="collider"></param>
        public void OnTriggerEnter(Collider collider)
        {
            script.fadeTopText(text, 2f);
            Destroy(this);
        }

    }
}
