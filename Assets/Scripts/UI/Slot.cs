﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

/// <summary>
/// a grid position in inventory that holds items
/// </summary>
/// 
/// <author>Frederik Mortensen</author>
namespace Askamaar
{

    public class Slot : MonoBehaviour
    {

        private int index;

        public int Index
        {
            get { return index; }
            set { index = value; }
        }

        private Stack<Item> stackItems;

        public Stack<Item> StackItems
        {
            get { return stackItems; }
            set { stackItems = value; }
        }

        public Text stackText;
        public Sprite emptySlot;
        public Sprite highlightedSlot;

        // Use this for initialization
        public void Awake()
        {
            stackItems = new Stack<Item>();
        }

        /// <summary>
        /// check if slot is empty
        /// </summary>
        public bool IsEmpty()
        {
            return stackItems.Count == 0;
        }

        /// <summary>
        /// add item to itemstack in slot and update stack text
        /// </summary>
        /// <param name="item"></param>
        public void AddItem(Item item)
        {
            stackItems.Push(item);

            if (stackItems.Count > 1)
            {
                stackText.text = stackItems.Count.ToString();
                stackText.color = Color.white;
            }

            ChangeSprite(item.neutralSprite, item.highlightedSprite);
        }

        /// <summary>
        /// needed for sorting and moving inside the inventory
        /// </summary>
        /// <param name="items"></param>
        public void AddItems(Stack<Item> items)
        {
            this.stackItems = new Stack<Item>(items);
            stackText.text = stackItems.Count > 1 ? stackItems.Count.ToString() : string.Empty;
            ChangeSprite(CurrentItem.neutralSprite, CurrentItem.highlightedSprite);
        }

        /// <summary>
        /// change stack image sprite to show item in inventory
        /// </summary>
        /// <param name="neutralSprite"></param>
        /// <param name="highlightedSprite"></param>
        private void ChangeSprite(Sprite neutralSprite, Sprite highlightedSprite)
        {
            GetComponent<Image>().sprite = neutralSprite;
            SpriteState spriteState = new SpriteState();
            spriteState.highlightedSprite = highlightedSprite;
            spriteState.pressedSprite = neutralSprite;
            GetComponent<Button>().spriteState = spriteState;
        }

        /// <summary>
        /// return the type of item that is in this slot
        /// </summary>
        public Item CurrentItem
        {
            get { return stackItems.Peek(); }
        }

        /// <summary>
        /// checks if there is still a stack place available or else other slot is needed to use
        /// </summary>
        /// <returns></returns>
        public bool IsAvailable
        {
            get { return CurrentItem.stackableSize > stackItems.Count; }
        }

        // <summary>
        // drink the health potion, stack weapon to player and stuff like that.
        // </summary>
        public void UseItem()
        {
            if (!IsEmpty())
            {
                Item item = stackItems.Pop();
                item.Use();

                stackText.text = stackItems.Count > 1 ? stackItems.Count.ToString() : string.Empty;

                if (IsEmpty())
                {
                    ChangeSprite(emptySlot, highlightedSlot);
                    InventoryScript.EmptySlots++;
                }
            }
        }

        /// <summary>
        /// items in this slot are removed, clear slot
        /// </summary>
        public void ClearSlot()
        {
            stackItems.Clear();
            ChangeSprite(emptySlot, highlightedSlot);
            stackText.text = string.Empty;
        }

    }
}
