﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// Handles loading and saving player progress (games)
/// </summary>
/// 
/// <author>Frederik Mortensen</author>
namespace Askamaar
{

    public class SaveGamesGuiScript : AbstractGridWindowScript
    {

        //force singleton
        public bool forceSingleInstance = true;
        private static SaveGamesGuiScript instance;
        public static SaveGamesGuiScript Instance
        {
            get
            {
                return instance;
            }
        }

        private SaveGameEntity[] saveGames;
        private SaveGameEntity selectedSaveGame;

        public SaveGameEntity SelectedSaveGame
        {
            get { return selectedSaveGame; }
        }

        public GameObject saveGamesHeader;
        private Text header; //SAVE GAME / LOAD GAME
        public bool saveLoadMode = false; //false = load, true = save
        private int slotPosition;

        /// <summary>
        /// initialize slot sizes, preview image and load the savegames
        /// </summary>
        public void Start()
        {

            //ensure that only one instance of this script is running
            if (forceSingleInstance && instance != null)
            {
                Debug.LogWarning("This singleton already exists!");
                Destroy(this);
            }
            else
            {
                instance = this;
            }

            PrefabName = "Prefabs/UI/MenuSlot"; //override default slot to use the menu button graphics

            if (!initialized)
            {
                //initially create slots and hold them as variable
                slots = 10;
                rows = 10;
                slotPaddingLeft = 10;
                slotPaddingTop = 10;
                slotWidth = 500;
                slotHeight = 50;
                textAlignForSlot = TextAnchor.MiddleLeft;
                CreateLayout();
                firstSlot.GetComponent<Button>().Select();
                initialized = true;
            }

            AddSaveGames(); //load persistent data
            SelectCurrentSaveGame(); //select first entry when loading dialog
            header = saveGamesHeader.GetComponent<Text>();
        }

        /// <summary>
        /// switch between save and load and handle user input
        /// </summary>
        public override void Update()
        {
            base.Update();
            if (saveLoadMode)
            {
                header.text = "SAVE GAME";
            }
            else
            {
                header.text = "LOAD GAME";
            }

            if (Input.GetButtonUp("PS4_CROSS") || Input.GetButtonUp("Submit"))
            {
                if (saveLoadMode)
                {
                    SaveGameController.Instance.SaveGame(slotPosition);
                }
                else
                {
                    SaveGameController.Instance.LoadGame(slotPosition);
                }
            }

        }

        /// <summary>
        /// fill slots with savegame information
        /// </summary>
        public void AddSaveGames()
        {
            //load savegames from file system
            saveGames = SaveGameController.Instance.LoadAllSaveGames();

            for (int i = 0; i < 10; i++)
            {
                Slot current = allSlots[i].GetComponent<Slot>();
                if (saveGames != null && saveGames[i] != null)
                {
                    current.stackText.text = saveGames[i].GetRepresentationString();
                }
                else
                {
                    current.stackText.text = "- empty -";
                }

            }
        }

        /// <summary>
        /// react on user input and get the selected savegame
        /// </summary>
        public override void HandleSelection()
        {
            SelectCurrentSaveGame();
        }

        /// <summary>
        /// set text color of selected savegame and show preview image
        /// </summary>
        private void SelectCurrentSaveGame()
        {
            //reset all text colors
            foreach (GameObject gameobject in allSlots)
            {
                gameobject.GetComponent<Slot>().stackText.color = Color.white;
            }

            GameObject current = EventSystem.current.currentSelectedGameObject;
            if (current != null)
            {
                //change text color of selected entry
                Slot slot = current.GetComponent<Slot>();
                slotPosition = slot.Index;
                if (saveGames[slot.Index] != null)
                {
                    selectedSaveGame = saveGames[slot.Index];
                }
                else
                {
                    selectedSaveGame = null;
                }
            }
        }

    }
}