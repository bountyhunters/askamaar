﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for inventory and other windows with slots
/// </summary>
/// 
/// <author>Frederik Mortensen</author>
namespace Askamaar
{

    public abstract class AbstractGridWindowScript : AbstractNavigableGui
    {

        protected RectTransform windowRectTransform;

        //calculated
        protected float windowWidth, windowHeight;

        protected int slots;

        protected int rows;

        protected float slotPaddingLeft, slotPaddingTop;

        protected float slotWidth;

        protected float slotHeight;

        //holds all bag places
        protected List<GameObject> allSlots;

        private static int emptySlots;

        public static int EmptySlots
        {
            get { return emptySlots; }
            set { emptySlots = value; }
        }

        protected bool initialized = false;

        private string prefabName = "Prefabs/UI/Slot";

        public string PrefabName
        {
            get { return prefabName; }
            set { prefabName = value; }
        }

        protected GameObject firstSlot;

        protected TextAnchor textAlignForSlot;

        /// <summary>
        /// draw a grid
        /// </summary>
        protected void CreateLayout()
        {
            SetWindowSize();
            AddSlots();
        }

        /// <summary>
        /// first set background size of parent window
        /// </summary>
        protected void SetWindowSize()
        {
            emptySlots = slots;

            //Plätze durch Reihen mal Einzelplatzgröße plus linkem Rand plus einmalig rechter Rand
            windowWidth = (slots / rows) * (slotWidth + slotPaddingLeft) + slotPaddingLeft;

            //selbe Logik für die Höhe
            windowHeight = rows * (slotHeight + slotPaddingTop) + slotPaddingTop;

            windowRectTransform = GetComponent<RectTransform>();
            windowRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, windowWidth);
            windowRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, windowHeight);
        }

        /// <summary>
        /// next add slots to window
        /// </summary>
        private void AddSlots()
        {
            allSlots = new List<GameObject>();
            int columns = slots / rows;

            int slotIndex = 0;

            for (int y = 0; y < rows; y++)
            {
                for (int x = 0; x < columns; x++)
                {
                    GameObject newSlot = Instantiate(Resources.Load(PrefabName) as GameObject);
                    RectTransform slotRect = newSlot.GetComponent<RectTransform>();
                    newSlot.name = "Slot(" + x + "," + y + ")";
                    newSlot.GetComponent<Slot>().Index = slotIndex;
                    newSlot.transform.SetParent(this.transform.parent);
                    float xPosition = slotPaddingLeft * (x + 1) + (slotWidth * x);
                    float yPosition = -slotPaddingTop * (y + 1) + -(slotHeight * y); //entlang der Y-Achse nach unten
                    newSlot.transform.localScale = Vector3.one;

                    slotRect.localPosition = windowRectTransform.localPosition + new Vector3(xPosition, yPosition);

                    //Kachelgröße
                    slotRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, slotWidth);
                    slotRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, slotHeight);

                    //slotText width, height and alignment
                    RectTransform textRect = newSlot.GetComponent<Slot>().stackText.GetComponent<RectTransform>();
                    newSlot.GetComponent<Slot>().stackText.alignment = textAlignForSlot;
                    textRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, slotWidth);
                    textRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, slotHeight);

                    allSlots.Add(newSlot);
                    if (x == 0 && y == 0)
                    {
                        firstSlot = newSlot;
                    }

                    slotIndex++;

                }
            }
        }

    }
}