﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// load preview image to according savegame
/// </summary>
/// 
/// <author>Frederik Mortensen</author>
namespace Askamaar
{

    public class SaveGamePreviewScript : MonoBehaviour
    {

        private Image saveGamePreviewImage;

        /// <summary>
        /// 
        /// </summary>
        public void Start()
        {
            saveGamePreviewImage = gameObject.GetComponent<Image>();
        }

        /// <summary>
        /// change preview image
        /// </summary>
        public void Update()
        {
            //TODO: buffering to prevent loading resources unneccesarily often
            SaveGameEntity saveGame = SaveGamesGuiScript.Instance.SelectedSaveGame;
            Texture2D texture = Resources.Load<Texture2D>(GetPreviewImage(saveGame != null ? saveGame.levelName : ""));
            Rect rect = new Rect(0, 0, texture.width, texture.height);
            Vector2 vector = new Vector2(0.5f, 0.5f);
            Sprite sprite = Sprite.Create(texture, rect, vector);
            saveGamePreviewImage.sprite = sprite;
        }

        /// <summary>
        /// holds the paths to the level preview images
        /// </summary>
        /// <param name="levelName"></param>
        /// <returns></returns>
        private string GetPreviewImage(string levelName)
        {
            switch (levelName)
            {
                //TODO: erweitern bei den neuen Levels
                case "FirstVillage":
                    return "Sprites/Levels/" + levelName;
                default:
                    return "Sprites/Levels/placeholder";
            }
        }

    }
}