﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// Script that handles text output like "Game Over" and "Pause".
/// </summary>
/// 
/// <author>Frederik Mortensen</author>
namespace Askamaar
{

    public class ScreenTextScript : MonoBehaviour
    {

        public Text topText;
        public Text centerText;

        /// <summary>
        /// initialize objects to use
        /// </summary>
        void Start()
        {
            Text text = gameObject.GetComponentInChildren<Text>();
            if (text.name.Equals("TopText"))
            {
                topText = text;
            }
            else if (text.name.Equals("CenterText"))
            {
                centerText = text;
            }
        }

        /// <summary>
        /// show the given text in the preferred screen area
        /// </summary>
        void Update()
        {

            //take care of level names and stuff...
            if (topText != null)
            {
                //todo...
            }

            //take care of pause, game over and others...
            if (centerText != null)
            {
                //if (levelMaster.IsGamePaused())
                //{
                //    if (centerText.text.Equals(""))
                //    {
                //        centerText.text = "Pause";
                //    }
                //}
                //else
                //{
                //    if (centerText.text.Equals("Pause"))
                //        centerText.text = "";
                //}
                //if (player.IsDead())
                //{
                //    centerText.text = "GAME OVER";
                //    return;
                //}
            }
        }

        /// <summary>
        /// fade out the level name or other things
        /// </summary>
        /// <param name="text">text to fade</param>
        /// <param name="initalWait">seconds to wait before fading</param>
        public void fadeTopText(string text, float initalWait)
        {
            topText.text = text;
            topText.color = Color.white;
            StartCoroutine(fadeTopTextCoRoutine(initalWait, 10f));
        }

        /// <summary>
        /// fade out the collected items and stuff like that...
        /// </summary>
        /// <param name="text">text to fade</param>
        /// <param name="initalWait">seconds to wait before fading</param>
        public void fadeCenterText(string text, float initalWait)
        {
            centerText.text = text;
            centerText.color = Color.white;
            StartCoroutine(fadeCenterTextCoRoutine(initalWait, 10f));
        }

        /// <summary>
        /// subroutine that fades the given text out after 2 seconds
        /// </summary>
        /// <returns></returns>
        private IEnumerator fadeTopTextCoRoutine(float initialWait, float totalTime)
        {
            yield return new WaitForSeconds(initialWait);
            float elapsedTime = 0;
            while (elapsedTime < 1)
            {
                Color startColor = topText.color;
                Color endColor = new Color(1, 1, 1, 0);
                topText.color = Color.Lerp(startColor, endColor, 0.2f);
                elapsedTime += Time.deltaTime / totalTime;
                yield return new WaitForSeconds(0.1f);
            }
        }

        /// <summary>
        /// subroutine that fades the given text out after 2 seconds
        /// </summary>
        /// <returns></returns>
        private IEnumerator fadeCenterTextCoRoutine(float initialWait, float totalTime)
        {
            yield return new WaitForSeconds(initialWait);
            float elapsedTime = 0;
            while (elapsedTime < 1)
            {
                Color startColor = centerText.color;
                Color endColor = new Color(1, 1, 1, 0);
                centerText.color = Color.Lerp(startColor, endColor, 0.2f);
                elapsedTime += Time.deltaTime / totalTime;
                yield return new WaitForSeconds(0.1f);
            }
        }

    }
}