﻿using UnityEngine;

/// <summary>
/// Helper class for item components used in the inventory
/// </summary>
/// 
/// <author>Frederik Mortensen</author>
namespace Askamaar
{

    public class ItemDatabase
    {

        /// <summary>
        /// Instantiates a gameobject of the given id and return its item component
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Item GetItemById(int id)
        {

            string prefabPath = null;

            switch(id)
            {
                //case 1:
                //    prefabPath = "Prefabs/SmallHealthPotion";
                //    break;
                //case 2:
                //    prefabPath = "Prefabs/LongSword";
                //    break;
                default:
                    return null;
            }

            GameObject go = GameObject.Instantiate(Resources.Load<GameObject>(prefabPath));
            Item item = go.GetComponent<Item>();
            GameObject.Destroy(go);
            return item;

        }


    }
}
