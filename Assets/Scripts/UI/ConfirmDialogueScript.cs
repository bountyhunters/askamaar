﻿using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// shows a yes / no dialogue. This class is planed to be reusable with minor changes.
/// </summary>
/// 
/// <author>Frederik Mortensen</author>
namespace Askamaar
{

    public class ConfirmDialogueScript : AbstractNavigableGui
    {

        /// <summary>
        /// handle user input
        /// </summary>
        public override void Update()
        {

            //pressed playstation x button
            if (Input.GetButtonUp("PS4_CROSS"))
            {
                SceneManager.LoadScene("MainMenu");
            }
            else if (Input.GetButtonUp("PS4_CIRCLE"))
            {
                UIControllerScript.Instance.GoBack();
            }
        }

    }
}