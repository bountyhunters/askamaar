﻿/// <summary>
/// All selectable things...
/// </summary>
/// 
/// <author>Frederik Mortensen</author>
namespace Askamaar
{
    public interface ISelectable
    {
        /// <summary>
        /// 
        /// </summary>
        void Select();

        /// <summary>
        /// 
        /// </summary>
        void DeSelect();
    }
}