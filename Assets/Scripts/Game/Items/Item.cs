﻿using UnityEngine;

/// <summary>
/// This item class represents objects the player can store in his inventory.
/// </summary>
/// <author>Frederik Mortensen</author>
namespace Askamaar
{
    public class Item : MonoBehaviour, ISaveable
    {
        public int id;
        public string uuid;
        public ItemType type;
        public string itemName;
        public Sprite neutralSprite; //as shown in the inventory
        public Sprite highlightedSprite; //as shown in the inventory
        public int stackableSize; //the max stackable number in the inventory
        public string prefabPath;

        /// <summary>
        /// define what to do with the item when used in inventory
        /// </summary>
        public virtual void Use()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetUUID()
        {
            return uuid;
        }
    }
}