﻿/// <summary>
/// Defines which kind of (possibly usable) item it is
/// </summary>
///
/// <author>Frederik Mortensen</author>
namespace Askamaar
{

    [System.Serializable]
    public enum ItemType
    {
        Weapon,
        Garment,
        Food,
        Potion,
        QuestItem,
        Key
    }
}