﻿using System.Collections;
using UnityEngine;

/// <summary>
/// 
/// </summary>
/// <author>Frederik Mortensen</author>
public class FlickeringFlame : MonoBehaviour
{

    float minFlickerValue = 0.1f;
    float maxFlickerValue = 0.5f;

    private Light attachedLight;

    /// <summary>
    /// 
    /// </summary>
    private void Start()
    {
        attachedLight = gameObject.GetComponent<Light>();
        StartCoroutine(FlickeringLight());
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    IEnumerator FlickeringLight()
    {
        while (true)
        {
            float randomValue = Random.Range(minFlickerValue, maxFlickerValue);
            attachedLight.intensity -= randomValue;
            yield return new WaitForSeconds(0.05f);
            attachedLight.intensity += randomValue;
            yield return new WaitForSeconds(0.05f);
        }
    }
}
