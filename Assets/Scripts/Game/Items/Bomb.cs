﻿using UnityEngine;

/// <summary>
/// Explosive stuff
/// </summary>
/// 
/// <author>Frederik Mortensen</author>
namespace Askamaar
{
    public class Bomb : MonoBehaviour, IExplodable
    {

        /// <summary>
        /// 
        /// </summary>
        void Start()
        {
            Invoke("Explode", 15);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="coll"></param>
        void OnCollisionEnter(Collision coll)
        {
            if (!coll.gameObject.name.Equals("Bomb") && coll.gameObject.GetComponent<IExplodable>() != null)
            {
                Explode();
            }
        }

        /// <summary>
        /// implements interface IExplodable
        /// </summary>
        public void Explode()
        {
            ParticleSystem explosion = GetComponent<ParticleSystem>();
            MeshRenderer render = GetComponentInChildren<MeshRenderer>();
            AudioSource audio = GetComponent<AudioSource>();
            render.enabled = false;
            audio.Play();
            explosion.Play();
            ApplySurroundingDamage(gameObject.transform.position, 2f);
            float duration = explosion.main.duration - 0.2f;
            Destroy(gameObject, duration);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="center"></param>
        /// <param name="radius"></param>
        void ApplySurroundingDamage(Vector3 center, float radius)
        {
            Collider[] hitColliders = Physics.OverlapSphere(center, radius);
            foreach (Collider collider in hitColliders)
            {
                if (collider.gameObject == gameObject)
                {
                    continue;
                }

                IExplodable[] explodables = collider.gameObject.GetComponents<IExplodable>();
                foreach (IExplodable explodable in explodables)
                {
                    if (!(explodable is Bomb))
                        explodable.Explode();
                }
            }
        }
    }
}