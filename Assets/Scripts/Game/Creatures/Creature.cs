﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Base class for players, npcs and enemies
/// </summary>
/// <author>Frederik Mortensen</author>
namespace Askamaar
{
    public abstract class Creature : MonoBehaviour, ISaveable
    {

        [SerializeField]
        public string uuid;

        [SerializeField]
        private int maxHealth;

        [SerializeField]
        private int health;

        [SerializeField]
        private bool godModeOn = false;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int GetMaxHealth()
        {
            return maxHealth;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="amount"></param>
        public void SetMaxHealth(int amount)
        {
            maxHealth = amount;
            if (health > maxHealth)
            {
                health = maxHealth;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int GetHealth()
        {
            return health;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="amount"></param>
        public void SetHealth(int amount)
        {
            health = maxHealth > 0 ? Mathf.Min(amount, maxHealth) : health;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="amount"></param>
        public virtual void ApplyDamage(int amount)
        {
            if (godModeOn)
            {
                return;
            }
            health = Mathf.Max(0, health - amount);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="amount"></param>
        public virtual void AddHealth(int amount)
        {
            health = Mathf.Min(health + amount, maxHealth);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool IsDead()
        {
            return health <= 0;
        }

        /// <summary>
        /// for generic purposes
        /// </summary>
        /// <returns></returns>
        public string GetUUID()
        {
            return uuid;
        }
    }
}