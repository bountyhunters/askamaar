﻿using System;

/// <summary>
/// This class helps solving the problem of communicating between scenes
/// </summary>
///
/// <author>Frederik Mortensen</author>
namespace Askamaar
{

    [Serializable]
    class PersistentGameState
    {

        public GameState gameState;
        public string value;

        //all states to react on after new scene was loaded
        public enum GameState
        {
            NONE,
            SAVEGAME_LOADED
        }

    }
}