﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

/// <summary>
/// This class handles all kinds of (binary) file saving and loading operations
/// </summary>
/// 
/// <author>Frederik Mortensen</author>
namespace Askamaar
{
    class PersistenceController : MonoBehaviour
    {

        private static PersistenceController instance;
        public static PersistenceController Instance
        {
            get
            {
                return instance;
            }
        }

        private BinaryFormatter binaryFormatter = new BinaryFormatter();
        public static string FILENAMEPREFIX;
        public static readonly string FILENAMESUFFIX = ".dat";
        public static readonly string FILENAMEGAMESTATE = "gsdata";

        /// <summary>
        /// create singleton instance
        /// </summary>
        public void Awake()
        {

            //must be called within main thread (awake or start, not in constructor)
            FILENAMEPREFIX = Application.persistentDataPath + "/askamaar";

            //ensure that only one instance of this script is running
            if (instance != null)
            {
                Debug.LogWarning("This singleton already exists!");
                Destroy(this);
            }
            else
            {
                instance = this;
            }
        }

        /// <summary>
        /// save object to disc
        /// </summary>
        /// <param name="objectToPersist"></param>
        /// <param name="fileName"></param>
        public void SaveObject(System.Object objectToPersist, string fileName)
        {
            //override existing or create new
            FileStream file = File.OpenWrite(FILENAMEPREFIX + fileName + FILENAMESUFFIX);
            binaryFormatter.Serialize(file, objectToPersist);
            file.Close();
        }

        /// <summary>
        /// load object from disc
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns>the loaded object as system object. It still has to be cast.</returns>
        public System.Object LoadObject(string fileName)
        {
            if (File.Exists(FILENAMEPREFIX + fileName + FILENAMESUFFIX))
            {
                FileStream file = File.Open(FILENAMEPREFIX + fileName + FILENAMESUFFIX, FileMode.Open);
                System.Object obj = binaryFormatter.Deserialize(file);
                file.Close();
                return obj;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// returns the game state that says wether the game was recently loading a savegame or anything else
        /// </summary>
        /// <returns>the entity object of persistent state</returns>
        public PersistentGameState LoadGameState()
        {
            PersistentGameState gameState = (PersistentGameState)LoadObject(FILENAMEGAMESTATE);
            if (gameState == null)
            {
                return new PersistentGameState();
            }
            return gameState;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameState"></param>
        public void SaveGameState(PersistentGameState gameState)
        {
            SaveObject(gameState, PersistenceController.FILENAMEGAMESTATE);
        }


        /// <summary>
        /// clear last states
        /// </summary>
        public void ResetGameState()
        {
            PersistentGameState gameState = LoadGameState();
            gameState.gameState = PersistentGameState.GameState.NONE;
            gameState.value = null;
            SaveObject(gameState, FILENAMEGAMESTATE);
        }

    }
}
