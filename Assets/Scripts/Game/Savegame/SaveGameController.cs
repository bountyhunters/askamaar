﻿using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// persist and load savegames
/// </summary>
/// 
/// <author>Frederik Mortensen</author>
namespace Askamaar
{

    class SaveGameController : MonoBehaviour
    {

        private static SaveGameController instance;
        public static SaveGameController Instance
        {
            get
            {
                return instance;
            }
        }

        /// <summary>
        /// create singleton instance
        /// </summary>
        public void Awake()
        {
            //ensure that only one instance of this script is running
            if (instance != null)
            {
                Debug.LogWarning("This singleton already exists!");
                Destroy(this);
            }
            else
            {
                instance = this;
            }

        }

        /// <summary>
        /// initialization
        /// </summary>
        public void Start()
        {

        }

        /// <summary>
        /// saves the changes to the scene into a binary file into slot "number"
        /// </summary>
        /// <param name="number"></param>
        public void SaveGame(int number)
        {
            //TODO: Check if slot exists / override?

            //fill savegame with the serializable data
            SaveGameEntity saveGame = new SaveGameEntity();

            //level name
            saveGame.levelName = SceneManager.GetActiveScene().name;

            //current time
            saveGame.date = System.DateTime.Now;

            //items
            saveGame.items = collectSceneObjectIdsByTag<Item>("Item");

            //inventory
            saveGame.inventorySlots = collectInventory();

            //player position
            GameObject player = GameController.Instance.playerInstance;
            Vector3 pos = player.transform.position;
            string playerPos = pos.x + ";" + pos.y + ";" + pos.z;
            saveGame.playerPos = playerPos;

            //save
            PersistenceController.Instance.SaveObject(saveGame, number.ToString());
        }

        /// <summary>
        /// get persisted data, deserialize it and load it into scene
        /// </summary>
        /// <param name="number">save game number</param>
        public void LoadGame(int number)
        {
            SaveGameEntity loadedSaveGame = (SaveGameEntity)PersistenceController.Instance.LoadObject(number.ToString());
            if (loadedSaveGame == null)
            {
                return; //nothing to do, stop loading
            }
            PersistentGameState gameState = PersistenceController.Instance.LoadGameState();
            gameState.gameState = PersistentGameState.GameState.SAVEGAME_LOADED;
            gameState.value = number.ToString();
            PersistenceController.Instance.SaveGameState(gameState);
            SceneManager.LoadScene(loadedSaveGame.levelName, LoadSceneMode.Single);
        }

        /// <summary>
        /// reads the savegames from filesystem
        /// </summary>
        /// <returns>all persistet save game entity objects</returns>
        public SaveGameEntity[] LoadAllSaveGames()
        {
            SaveGameEntity[] saveGames = new SaveGameEntity[10];
            for (int i = 0; i < 10; i++)
            {
                saveGames[i] = (SaveGameEntity)PersistenceController.Instance.LoadObject(i.ToString());
            }
            return saveGames;
        }

        /// <summary>
        /// searches the current scene for objects with the given tag, then gets their ids,
        /// and add them to a comma separated string. Return this string for serialization.
        /// </summary>
        /// <param name="tag"></param>
        /// <returns>comma separated ids of the objects found</returns>
        private string collectSceneObjectIdsByTag<T>(string tag) where T : ISaveable
        {
            StringBuilder commaSeparatedItems = new StringBuilder();

            GameObject[] objects = GameObject.FindGameObjectsWithTag(tag);
            foreach (GameObject obj in objects)
            {
                if (obj.GetComponent<T>() != null)
                {
                    if (commaSeparatedItems.Length > 0)
                    {
                        commaSeparatedItems.Append(",");
                    }
                    commaSeparatedItems.Append(obj.GetComponent<T>().GetUUID());
                }
            }

            return commaSeparatedItems.ToString();
        }

        /// <summary>
        /// get all inventory stuff as string arrays
        /// </summary>
        /// <returns>serialized inventory item data</returns>
        private string[] collectInventory()
        {
            // Set inventory instance to player
            string[] result = UIControllerScript.Instance.inventory.GetAllItems();
            return result;
        }

        /// <summary>
        /// loads the given savegame from disc and returns it
        /// </summary>
        /// <param name="number"></param>
        /// <returns>the persistet save game entity as object</returns>
        private SaveGameEntity GetSaveGame(int number)
        {
            return (SaveGameEntity)PersistenceController.Instance.LoadObject(number.ToString());
        }

        /// <summary>
        /// add a delegate to the scene manager to call custom methods after a scene change
        /// </summary>
        void OnEnable()
        {
            SceneManager.sceneLoaded += OnLevelFinishedLoading;
        }

        /// <summary>
        /// remove the delegate from the scene manager again. must be done with all delegates
        /// </summary>
        void OnDisable()
        {
            SceneManager.sceneLoaded -= OnLevelFinishedLoading;
        }

        /// <summary>
        /// everytime a new level / scene was loaded the gamestate is checked wether there
        /// is something else to do. Like loading and applying savegame changes to the new
        /// scene.
        /// </summary>
        /// <param name="scene"></param>
        /// <param name="mode"></param>
        void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
        {
            PersistentGameState state = PersistenceController.Instance.LoadGameState();
            if (state.gameState == PersistentGameState.GameState.SAVEGAME_LOADED)
            {
                int levelNumber = int.Parse(state.value);
                ApplySaveGameChanges(levelNumber);
            }
        }

        /// <summary>
        /// gets the persistent savegame after the scene was loaded and changes the state
        /// of objects, enemies player and stuff to fitt the savegame state.
        /// </summary>
        /// <param name="number">number of savegame from 0 to 9</param>
        public void ApplySaveGameChanges(int number)
        {
            SaveGameEntity saveGame = GetSaveGame(number);

            //Items
            string[] saveGameItems = saveGame.items.Split(',');
            Item[] sceneItems = GameObject.FindObjectsOfType<Item>();
            foreach (Item item in sceneItems)
            {
                bool found = false;
                foreach (string saveGameItem in saveGameItems)
                {
                    if (saveGameItem.Equals(item.uuid))
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                    GameObject.Destroy(item.gameObject);
            }

            //Inventory
            string[] inventorySlots = saveGame.inventorySlots;
            UIControllerScript.Instance.fillInventory(inventorySlots);

            //Player Position
            string playerPos = saveGame.playerPos;
            string[] coordinates = playerPos.Split(';');
            float x = float.Parse(coordinates[0]);
            float y = float.Parse(coordinates[1]);
            float z = float.Parse(coordinates[2]);
            GameObject spawnPoint = GameObject.Find("Spawn Point");
            spawnPoint.transform.position = new Vector3(x, y + 2, z);
            GameController.Instance.playerInstance.transform.position = spawnPoint.transform.position;
            PersistenceController.Instance.ResetGameState();
        }

    }
}