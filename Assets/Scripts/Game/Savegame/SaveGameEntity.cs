﻿using System;

/// <summary>
/// Class containing all game data to be saved and loaded
/// </summary>
/// 
/// <author>Frederik Mortensen</author>
namespace Askamaar
{

    [Serializable]
    public class SaveGameEntity
    {
        public string levelName; //the level the player is currently in

        public DateTime date; //savegame time

        public string items; //comma separated uuids of the items in the scene

        public string[] inventorySlots; //inventory items

        public string playerPos;

        //public string creatures;

        /// <summary>
        /// returns the string as shown in the load- and save game dialog
        /// </summary>
        /// <returns>scene + date</returns>
        public string GetRepresentationString()
        {
            return levelName + " " + date.ToString("yyyy-MM-dd HH:mm:ss");
        }

    }
}