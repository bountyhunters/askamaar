﻿using UnityEngine;

/// <summary>
/// Helper to instanciate prefabs
/// </summary>
/// 
/// <author>Frederik Mortensen</author>
namespace Askamaar
{
    public class PrefabController : MonoBehaviour
    {

        //game objects
        public GameObject player;
        public GameObject bomb;

        private static PrefabController instance;
        public static PrefabController Instance
        {
            get
            {
                return instance;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void Awake()
        {
            //ensure that only one instance of this script is running
            if (instance != null)
            {
                Debug.LogWarning("This singleton already exists!");
                Destroy(this);
            }
            else
            {
                instance = this;
            }
        }
    }
}