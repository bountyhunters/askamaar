﻿using UnityEngine;

/// <summary>
/// Game related data like game state. Holds player.
/// </summary>
/// 
/// <author>Frederik Mortensen</author>
namespace Askamaar
{
    public class GameController : MonoBehaviour
    {

        public GameObject playerInstance;

        public static int GAME_STATE_NOT_LOADED = 0;
        public static int GAME_STATE_ACTIVE = 1;
        public static int GAME_STATE_GAMEOVER = 2;

        public int currentGameState = GAME_STATE_NOT_LOADED;

        private static GameController instance;
        public static GameController Instance
        {
            get
            {
                return instance;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void Awake()
        {
            //ensure that only one instance of this script is running
            if (instance != null)
            {
                Debug.LogWarning("This singleton already exists!");
                Destroy(this);
            }
            else
            {
                instance = this;
            }

            SetupGame();

        }

        /// <summary>
        /// 
        /// </summary>
        private void SetupGame()
        {
            playerInstance = GameObject.Find("PlayerController");
            GameObject spawnPoint = GameObject.Find("Spawn Point");
            playerInstance.transform.position = spawnPoint.transform.position;

            currentGameState = GAME_STATE_ACTIVE;
        }

    }
}