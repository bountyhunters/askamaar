﻿using UnityEngine;

/// <summary>
/// All things camera. Binds camera to player
/// </summary>
/// 
/// <author>Frederik Mortensen</author>
namespace Askamaar
{
    public class CameraController : MonoBehaviour
    {

        private Vector3 offset;
        private GameObject player;

        void LateUpdate()
        {
            if (player == null)
            {
                player = GameController.Instance.playerInstance;
                offset = new Vector3(0, 10f, -6);
            }

            transform.position = player.transform.position + offset;
            transform.rotation = player.transform.rotation;
        }
    }
}