﻿/// <summary>
/// Flags explodable items. Helps searching and stuff.
/// </summary>
/// 
/// <author>Frederik Mortensen</author>
namespace Askamaar
{
    public interface IExplodable
    {

        /// <summary>
        /// 
        /// </summary>
        void Explode();
    }

}