﻿/// <summary>
/// Interface for all objects that can be persisted to and loaded from savegames.
/// </summary>
/// 
/// <author>Frederik Mortensen</author>
namespace Askamaar
{
    public interface ISaveable
    {
        string GetUUID();
    }
}