﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 
/// </summary>
/// 
///<author>Frederik Mortensen</author>
namespace Askamaar
{
    public class Sunlight : MonoBehaviour
    {

        Material sky;
        public Renderer water;
        public Transform stars;
        public Transform worldProbe;

        /// <summary>
        /// 
        /// </summary>
        void Start()
        {
            sky = RenderSettings.skybox;
        }

        /// <summary>
        /// 
        /// </summary>
        void Update()
        {
            stars.transform.rotation = transform.rotation;
            Vector3 tvec = Camera.main.transform.position;
            if (water != null)
            {
                worldProbe.transform.position = tvec;
                water.material.mainTextureOffset = new Vector2(Time.time / 100, 0);
                water.material.SetTextureOffset("_DetailAlbedoMap", new Vector2(0, Time.time / 80));
            }
        }
    }
}