﻿using UnityEngine;

/// <summary>
/// 
/// </summary>
/// 
///<author>Frederik Mortensen</author>
namespace Askamaar
{
    public class AutoIntensity : MonoBehaviour
    {

        public Gradient nightDayColor;

        public float maxIntensity = 1f;
        public float minIntensity = 0.2f;
        public float minPoint = -0.2f;

        public float maxAmbient = 0.5f;
        public float minAmbient = 0.2f;
        public float minAmbientPoint = -0.2f;

        public Gradient nightDayFogColor;
        public AnimationCurve fogDensityCurve;
        public float fogScale = 1f;

        public float dayAtmosphereThickness = 0.4f;
        public float nightAtmosphereThickness = 0.87f;

        public Vector3 dayRotateSpeed;
        public Vector3 nightRotateSpeed;

        public float skySpeed = 0.05f;

        Light mainLight;
        Material skyMat;

        private bool debugEnabled = false;

        /// <summary>
        /// 
        /// </summary>
        void Start()
        {
            mainLight = GetComponent<Light>();
            skyMat = RenderSettings.skybox;
            //SetEarlyMorningTime();
            //SetMorningTime();
            SetMiddayTime();
            //SetEveningTime();
            //SetNightTime();
        }

        /// <summary>
        /// 
        /// </summary>
        void Update()
        {
            float tRange = 1 - minPoint;
            float dot = Mathf.Clamp01((Vector3.Dot(mainLight.transform.forward, Vector3.down) - minPoint) / tRange);
            float i = ((maxIntensity - minIntensity) * dot) + minIntensity;

            mainLight.intensity = i;

            tRange = 1 - minAmbientPoint;
            dot = Mathf.Clamp01((Vector3.Dot(mainLight.transform.forward, Vector3.down) - minAmbientPoint) / tRange);
            i = ((maxAmbient - minAmbient) * dot) + minAmbient;
            RenderSettings.ambientIntensity = i;

            mainLight.color = nightDayColor.Evaluate(dot);
            RenderSettings.ambientLight = mainLight.color;

            RenderSettings.fogColor = nightDayFogColor.Evaluate(dot) * fogScale;

            i = ((dayAtmosphereThickness - nightAtmosphereThickness) * dot) + nightAtmosphereThickness;
            skyMat.SetFloat("_AtmosphereThickness", i);

            if (dot > 0)
            {
                transform.Rotate(dayRotateSpeed * Time.deltaTime * skySpeed);
            }
            else
            {
                transform.Rotate(nightRotateSpeed * Time.deltaTime * skySpeed);
            }

            if (Input.GetKeyDown(KeyCode.Q)) skySpeed *= 0.5f;
            if (Input.GetKeyDown(KeyCode.E)) skySpeed *= 2f;

            if (debugEnabled)
            {
                Debug.Log("mainLight.transform.rotation: " + mainLight.transform.rotation);
                Debug.Log("mainLight.intensity: " + mainLight.intensity);
                Debug.Log("RenderSettings.ambientIntensity: " + RenderSettings.ambientIntensity);
                Debug.Log("mainLight.color: " + mainLight.color);
                Debug.Log("RenderSettings.ambientLight: " + RenderSettings.ambientLight);
                Debug.Log("RenderSettings.fogColor: " + RenderSettings.fogColor);
                Debug.Log("skyMat.SetFloat(\"_AtmosphereThickness\", i " + i);
                Debug.Log("transform.rotation: " + transform.rotation);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void SetEarlyMorningTime()
        {
            mainLight.transform.rotation = new Quaternion(-0.7f, -0.1f, 0.7f, -0.1f);
            mainLight.intensity = 0.385297f;
            RenderSettings.ambientIntensity = 0.4448356f;
            mainLight.color = new Color(0.984f, 0.451f, 0.427f, 1.000f);
            RenderSettings.ambientLight = new Color(0.984f, 0.451f, 0.427f, 1.000f);
            RenderSettings.fogColor = new Color(0.529f, 0.518f, 0.231f, 1.000f);
            skyMat.SetFloat("_AtmosphereThickness", 1.347058f);
            transform.rotation = new Quaternion(0.7f, 0.0f, -0.7f, 0.0f);
        }

        /// <summary>
        /// 
        /// </summary>
        public void SetMorningTime()
        {
            mainLight.transform.rotation = new Quaternion(-0.7f, -0.1f, 0.7f, -0.1f);
            mainLight.intensity = 0.4448356f;
            RenderSettings.ambientIntensity = 0.4448356f;
            mainLight.color = new Color(1.000f, 0.980f, 0.310f, 1.000f);
            RenderSettings.ambientLight = new Color(1.000f, 0.980f, 0.310f, 1.000f);
            RenderSettings.fogColor = new Color(0.749f, 0.718f, 0.380f, 1.000f);
            skyMat.SetFloat("_AtmosphereThickness", 1.330047f);
            transform.rotation = new Quaternion(-0.7f, -0.1f, 0.7f, -0.1f);
        }

        /// <summary>
        /// 
        /// </summary>
        public void SetMiddayTime()
        {
            mainLight.transform.rotation = new Quaternion(-0.4f, -0.6f, 0.4f, -0.6f);
            mainLight.intensity = 0.8382257f;
            RenderSettings.ambientIntensity = 0.8382257f;
            //mainLight.color = new Color(1.000f, 1.000f, 1.000f, 1.000f);
            mainLight.color = new Color(0.5f, 0.5f, 0.5f, 0.5f);
            RenderSettings.ambientLight = new Color(Color.yellow.r, Color.yellow.g, Color.yellow.b, 0.5f);
            RenderSettings.fogColor = new Color(0.169f, 0.573f, 0.855f, 1.000f);
            skyMat.SetFloat("_AtmosphereThickness", 1.21765f);
            transform.rotation = new Quaternion(-0.4f, -0.6f, 0.4f, -0.6f);
        }

        /// <summary>
        /// 
        /// </summary>
        public void SetEveningTime()
        {
            mainLight.transform.rotation = new Quaternion(-0.1f, -0.7f, 0.1f, -0.7f);
            mainLight.intensity = 0.4508563f;
            RenderSettings.ambientIntensity = 0.4508563f;
            mainLight.color = new Color(1.000f, 0.980f, 0.337f, 1.000f);
            RenderSettings.ambientLight = new Color(1.000f, 0.980f, 0.337f, 1.000f);
            RenderSettings.fogColor = new Color(0.773f, 0.737f, 0.392f, 1.000f);
            skyMat.SetFloat("_AtmosphereThickness", 1.328327f);
            transform.rotation = new Quaternion(-0.1f, -0.7f, 0.1f, -0.7f);
        }

        /// <summary>
        /// 
        /// </summary>
        public void SetNightTime()
        {
            mainLight.transform.rotation = new Quaternion(0.1f, -0.7f, -0.1f, -0.7f);
            mainLight.intensity = 0.2f;
            RenderSettings.ambientIntensity = 0.2f;
            mainLight.color = new Color(0.173f, 0.576f, 0.859f, 1.000f);
            RenderSettings.ambientLight = new Color(0.173f, 0.576f, 0.859f, 1.000f);
            RenderSettings.fogColor = new Color(0.000f, 0.000f, 0.000f, 1.000f);
            skyMat.SetFloat("_AtmosphereThickness", 1.4f);
            transform.rotation = new Quaternion(0.1f, -0.7f, -0.1f, -0.7f);
        }
    }
}