﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// Generates ids for game entities to refer to them in savegames.
/// </summary>
public class UUIDGeneratorMenu : MonoBehaviour
{

    /// <summary>
    /// Context menu entry in inspector
    /// </summary>
    /// <param name="command"></param>
    [MenuItem("CONTEXT/Building/Generate UUID", false, 20)]
    static void GenerateItemUUID(MenuCommand command)
    {
        //Building building = (Building)command.context;
        //if (string.IsNullOrEmpty(building.uuid))
        //{
        //    building.uuid = System.Guid.NewGuid().ToString();
        //    EditorUtility.SetDirty(building);
        //    Debug.Log("Generated uuid " + building.uuid);
        //}
    }

    /// <summary>
    /// hierarchy context menu and GameObject Menu entry
    /// </summary>
    /// <param name="menuCommand"></param>
    [MenuItem("GameObject/Askamaar/Generate UUID %u", false, 10)]
    static void GenerateUUIDOnGameObject(MenuCommand menuCommand)
    {
        //foreach (Transform transform in Selection.transforms)
        //{
        //    GameObject target = transform.gameObject;
        //    if (target.GetComponent<Building>() != null)
        //    {
        //        Building building = target.GetComponent<Building>();
        //        if (string.IsNullOrEmpty(building.uuid))
        //        {
        //            building.uuid = System.Guid.NewGuid().ToString();
        //            EditorUtility.SetDirty(building);
        //            Debug.Log("Generated uuid " + building.uuid);
        //        }
        //    }
        //}
    }

    /// <summary>
    /// Validate the menu item defined by the function above.
    /// The menu item will be disabled if this function returns false.
    /// </summary>
    /// <returns>true = activate menu, false = deactivate</returns>
    [MenuItem("GameObject/Askamaar/Generate UUID %u", true)]
    static bool ValidateGenerateUUIDOnGameObject()
    {
        if (Selection.transforms == null)
        {
            return false;
        }
        else if (Selection.transforms.Length == 0)
        {
            return false;
        }
        else if (Selection.transforms.Length > 0)
        {
            foreach (Transform transform in Selection.transforms)
            {
                //if (transform.GetComponent<Building>() == null)
                //{
                //    return false;
                //}
            }
            return true;
        }
        return false;
    }

}